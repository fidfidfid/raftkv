# Implementasi Raft dengan Typescript

Disalin dari Typescript-Node-Starter oleh Microsoft: https://github.com/Microsoft/TypeScript-Node-Starter/

Rujukan:

- Presentasi Raft (disederhanakan): http://thesecretlivesofdata.com/raft/
- Paper Raft: https://raft.github.io/raft.pdf

Cara menjalankan:

    npm install
    npm run build
    PORT=3000 npm start

Daftar pengerjaan:

- [X] Naive KV server
- [X] Implementasi assignment log
- [ ] Follower Timeout
- [ ] Cluster leader election
- [ ] Log replication
