"use strict";

import { Request, Response } from "express";
import * as naiveStorage from "../core/storage";

export let getEntry = (req: Request, res: Response) => {
  const key: string = req.params["key"];

  const value = naiveStorage.getEntry(key);
  res.json({key: key, value: value});
};

export let getEntries = (req: Request, res: Response) => {
  res.json(naiveStorage.getEntries());
};

export let setEntry = (req: Request, res: Response) => {
  const key: string = req.body.key;
  const value: string = req.body.value;

  const setEntry = naiveStorage.setEntry(key, value);
  res.json({key: key, value: value, setSucess: setEntry});
};

