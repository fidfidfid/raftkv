"use strict";

const dict: {[index: string]: string} = {};
const assignmentLogs: [ {[index: string]: string } ] = [{"_": ""}];

delete assignmentLogs[0];

export let getEntry = (key: string) => {
  return dict[key];
};

export let getEntries = () => {
  return dict;
};

export let setEntry = (key: string, value: string) => {
  dict[key] = value;
  const logToPush: {[index:string] : string} = {};
  logToPush[key] = value;

  assignmentLogs.push(logToPush);
  return true;
};
