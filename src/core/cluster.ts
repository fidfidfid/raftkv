/// Raft Cluster based on original extended paper https://raft.github.io/raft.pdf

import request from "request";

enum NodeState {
  Follower,
  Candidate,
  Leader
}

class NodeNetwork {
  rpcUrl: string;

  callAppendEntriesRPC(params: AppendEntriesParameter) {
    request.post(`${this.rpcUrl}`);
  }

  callAskForVote(nodeUrl: string) {
    request.post(`${this.rpcUrl}/cluster/askForVote`, {form: {voteFor: nodeUrl}});
  }
}

class ClusterStateOnNode {
  participation: NodeState;

  // Permanent state
  currentTerm: number;
  votedFor: number;
  logs: [{ string : string }];

  // Volatile State

  // Volatile state, only on leader

  nodes: [ NodeNetwork ];
  selfRpcUrl: string;
}

class AppendEntriesParameter {

}

export class ClusterEngine {
  nodeState: NodeState;
  clusterState: ClusterStateOnNode;
  timeoutHandler: number;

  constructor() {
    this.nodeState = NodeState.Follower;
  }

  start() {
    this.timeoutHandler = setTimeout(this.becomeCandidate);
  }

  becomeCandidate() {
    this.nodeState = NodeState.Candidate;

  }

  becomeLeader() {
    setTimeout(this.idleHeartBeat, 50);
  }

  state: () => {

  }

  idleHeartBeat() {
    // RPC to nodes
  }

  setTimeout: () => {

  }

  selfRPCHandler: () => {

  }

  requestVoteRPCHandler() {

  }

  appendEntriesRPCHandler(params: AppendEntriesParameter) {

  }
}

export let clusterEngine = new ClusterEngine();
